const express = require("express");
const path = require("path");

const config = require("./config/prodServerConfig.js");

const port = process.env.PORT || config.port;
const indexPage = path.join(__dirname, "./build", "index.html");

const app = express();

app.get("/status", (req, res) => {
  res.sendStatus(200);
});

app.use(express.static("./build"));

app.get("/*", (_req, res) => {
  res.sendFile(indexPage);
});

app.listen(port, (err) => {
  if (err) {
    console.log(err);
    return null;
  }
  console.log(
    `React typescript is listening on port ${port} with env ${app.get("env")}.`
  );
  return;
});
