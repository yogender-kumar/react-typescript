import { INews, IBaseAction } from "../types";

export const actionIds = {
  GET_NEWS_REQUEST_START: "[0] Request news async service.",
  GET_NEWS_REQUEST_COMPLETED: "[1] news async service completed",
  GET_NEWS_REQUEST_ERROR: "[2} news async service error",
};

export const fetchNewsStartAction = () => ({
  type: actionIds.GET_NEWS_REQUEST_START,
});

export const fetchNewsSuccessAction = (newsList: INews): IBaseAction => ({
  type: actionIds.GET_NEWS_REQUEST_COMPLETED,
  payload: newsList,
});
