import React, { Component, ReactNode } from 'react';
import styles from './ErrorBoundary.module.css';

type Props = {
  children: ReactNode;
};

type State = {
  hasError: boolean;
};

export default class ErrorBoundary extends Component<Props> {
  static getDerivedStateFromError(error: Error) {
    return { hasError: !!error };
  }

  state: State = {
    hasError: false,
  };

  componentDidCatch({ message, stack }: Error) {
    console.log('Something went wrong', message, stack)
  }

  render() {
    const { children } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      return (
        <div className={styles.root}>
          <h1>Something went wrong</h1>
        </div>
      );
    }

    return children;
  }
}
