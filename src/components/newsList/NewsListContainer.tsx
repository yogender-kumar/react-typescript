import { connect } from 'react-redux';

import { fetchNewsStartAction } from '../../actions';
import { NewsList } from './NewsList';

const mapStateToProps = (state: any) => ({
    data: state.newsCollection.data,
    loading: state.newsCollection.loading,
    error: state.newsCollection.error,
});

const mapDispatchToProps = (dispatch: any) => ({
    getNews: () => dispatch(fetchNewsStartAction()),
});

export const NewsListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(NewsList);