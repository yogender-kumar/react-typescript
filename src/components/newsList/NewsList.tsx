import * as React from 'react';
import { useDispatch } from 'react-redux';

import { IObject } from "../../types";

import styles from './NewsList.module.css';


interface Props {
    data: IObject,
    loading: boolean,
    error: any,
    getNews: () => void;
}

export const NewsList: React.FunctionComponent<Props> = props => {
    const { getNews, loading, error, data } = props;

    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(getNews());
    }, []);



    return (
        <div className={styles.wrapper}>
            {loading && <div className={styles.loading}>Loading...</div>}
            {error && <div className={styles.error}>Something went wrong!</div>}
            {data && data.hits && <ul>
                {data.hits.map(({ title = '', author = '' }) => <li><div>{title}<sub>{author}</sub></div></li>)}
            </ul>}
        </div>
    );
};
