import React from 'react';
import { Store } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';


import ErrorBoundary from '../errorBoundary/ErrorBoundary';
import AppContainer from '../app/AppContainer';

type Props = {
    store: Store;
};

export default ({ store }: Props) => {
    return (
        <ErrorBoundary>
            <Provider store={store}>
                <BrowserRouter>
                    <Route path="/" component={AppContainer} />
                </BrowserRouter>
            </Provider>
        </ErrorBoundary>
    );
};
