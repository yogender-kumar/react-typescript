import React from 'react';

import { NewsListContainer } from '../newsList/NewsListContainer';

const AppContainer = () => {

    return <div className="main-container"><NewsListContainer /></div>;
};
export default AppContainer;
