import { combineReducers } from "redux";
import { newsCollectionReducer, NewsCollectionState } from "./news.reducer";

export interface State {
  newsCollection: NewsCollectionState;
}

export const rootReducers = combineReducers<State>({
  newsCollection: newsCollectionReducer,
});
