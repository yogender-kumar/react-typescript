import { IBaseAction, IObject } from "../types";
import { actionIds } from "../actions";

export type NewsCollectionState = {
  data: IObject;
  loading: boolean;
  error: any;
};

export const newsCollectionReducer = (
  state: NewsCollectionState = {
    data: {},
    loading: false,
    error: "",
  },
  action: IBaseAction
) => {
  switch (action.type) {
    case actionIds.GET_NEWS_REQUEST_START:
      return { ...state, loading: true, error: "" };
    case actionIds.GET_NEWS_REQUEST_ERROR:
      return { ...state, loading: false, error: new Error() };
    case actionIds.GET_NEWS_REQUEST_COMPLETED:
      return { ...state, data: action.payload, loading: false, error: "" };
  }
  return state;
};
