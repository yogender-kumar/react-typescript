export interface IBaseAction {
  type: string;
  payload?: any;
}

export interface IObject {
  [key: string]: any;
}
