import { all, fork } from "redux-saga/effects";
import { newsRequestStarted } from "./news.sagas";

export const rootSaga = function* root() {
  yield all([newsRequestStarted()]);
};
