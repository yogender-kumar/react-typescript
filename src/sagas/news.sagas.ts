import { call, put, takeLatest } from "redux-saga/effects";
import apiCaller from "../utils";
import { actionIds } from "../actions";

function* fetchNews() {
  try {
    const news = yield call(apiCaller, "get", "/v1/search?tags=front_page");
    yield put({ type: actionIds.GET_NEWS_REQUEST_COMPLETED, data: news });
  } catch (e) {
    yield put({ type: actionIds.GET_NEWS_REQUEST_ERROR, error: e.message });
  }
}

export function* newsRequestStarted() {
  yield takeLatest(actionIds.GET_NEWS_REQUEST_START, fetchNews);
}
