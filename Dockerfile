FROM node:erbium

WORKDIR /usr/src/app

COPY yarn.lock package.json tsconfig.json webpack.config.js ./


RUN yarn install --production=false --non-interactive

COPY src src
COPY public public
COPY config config

COPY prodServer.js ./

RUN yarn prod

EXPOSE 8080
CMD [ "yarn", "server" ]
